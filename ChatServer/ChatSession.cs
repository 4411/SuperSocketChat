﻿using SuperSocket.SocketBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatServerLib
{
    /// <summary>
    /// 连接会话
    /// </summary>
    public class ChatSession:AppSession<ChatSession>
    {
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName
        {
            get;
            set;
        }


        protected override void OnSessionStarted()
        {
            base.OnSessionStarted();
        }

        protected override void OnSessionClosed(CloseReason reason)
        {
            base.OnSessionClosed(reason);
            var sessions = this.AppServer.GetSessions(_ => !string.IsNullOrWhiteSpace(_.UserName));
            foreach (var item in sessions)
            {
                item.Send(string.Format("User [{0}] quit.", this.UserName));
            }
        }
    }
}
