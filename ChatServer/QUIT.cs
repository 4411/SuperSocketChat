﻿using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatServerLib
{
    public class QUIT : CommandBase<ChatSession, StringRequestInfo>
    {
        /// <summary>
        /// 用户退出聊天
        /// </summary>
        /// <param name="session"></param>
        /// <param name="requestInfo"></param>
        public override void ExecuteCommand(ChatSession session, StringRequestInfo requestInfo)
        {
            session.Close(SuperSocket.SocketBase.CloseReason.ClientClosing);
        }
    }
}
