﻿using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatServerLib
{
    public class ONE2ONE : CommandBase<ChatSession, StringRequestInfo>
    {
        /// <summary>
        /// 一对一发消息
        /// </summary>
        /// <param name="session"></param>
        /// <param name="requestInfo"></param>
        public override void ExecuteCommand(ChatSession session, StringRequestInfo requestInfo)
        {
            if (string.IsNullOrWhiteSpace(requestInfo.Body))
            {
                session.Send("Invaid data.\r\nUse: ONE2ONE toNickName message");
                return;
            }
            string toUserName = requestInfo.Body.Substring(0, requestInfo.Body.IndexOf(' '));
            string message = requestInfo.Body.Substring(requestInfo.Body.IndexOf(' '));

            var toSession = session.AppServer.GetSessions(_ => _.UserName.Equals(toUserName, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            if (toSession == null)
            {
                session.Send(string.Format("User [{0}] is not online.", toUserName));
                return;
            }

            toSession.Send(string.Format("[{0}] {1}", session.UserName, message));
        }
    }
}
