﻿using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatServerLib
{
    /// <summary>
    /// 群发消息
    /// </summary>
    public class ONE2ALL : CommandBase<ChatSession, StringRequestInfo>
    {

        public override void ExecuteCommand(ChatSession session, StringRequestInfo requestInfo)
        {
            var sessions = session.AppServer.GetSessions(_ => !string.IsNullOrWhiteSpace(_.UserName) && _.SessionID != session.SessionID);

            string msg = string.Format("[{0}] {1}", session.UserName, requestInfo.Body);
            foreach (var item in sessions)
            {
                item.Send(msg);
            }
        }
    }
}
